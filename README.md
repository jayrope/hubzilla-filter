# Hubzilla - Filter
A general filter add-on for Hubzilla.

## Description
This add-on filters Hubzilla posts for terms and replaces the original posts with a notification in your stream. Th notification can be clicked to expand to the original post.

## Installation
Copy code to /extend/addon/hzaddons, effectively creating a "filter" folder there.
In terminal, navigate to your Hubzilla site root folder.
Run `util/udall` to create a sym-link in your /addon folder.

## Roadmap
- exempt poster (at)handles and domain names from filtering.
- instead of displaying a collapsed button within a filtered post, collapse the whole post into a clickable, minimized, one-line format, mentioning the poster name and filtered (category)/term. Clicking this expands the original post.
Later stages:
- Sort filter terms alphabetically, automatically.
- Define "exact term" (for instance by using quotes around it) or wildcard, like _ant_ is in "eleph_ant_".
- Sort filter terms into mutable & deletable categories containing multiple terms. Muting would mean to preserve the information, but exempt category from filtering, when muted. Deleting would kill th catgory incl. all terms in it. Needs conformation dialog upon deltion for sure.


## Contributing
Clone and develop yourself. Would be nice to hear about your findings.

## Authors and acknowledgment
Only starting: @jayrope

## License
Take it, use it. No service. Starting from original code to be found with Hubzillas NSFW plugin.
See 

## Project status
In progress.
